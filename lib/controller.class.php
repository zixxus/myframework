<?php

class Controller {

    protected $_model;
    protected $_controller;
    protected $_aciton;
    protected $_template;

    function __construct($model, $controller, $action) {
        if($model == null){
            $model = 'Item';
            $controller = 'items';
            $action = 'viewall';
        }
        $this->_controller = $controller;
        $this->_aciton = $action;
        $this->_model = $model;

        $this->$model =& new $model;
        $this->_template =& new Template($controller, $action);
    }

    function set($name, $val) {
        $this->_template->set($name, $val);
    }

    function __destruct() {
        $this->_template->render();
    }

}
