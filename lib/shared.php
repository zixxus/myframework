<?php

function Reportview(){
    if(DEVELOPMENT_ENVIRONMENT == true){
        error_reporting(E_ALL);
        ini_set('display_errors', 'On');
    }  else {
        error_reporting(E_ALL);
        ini_set('display_errors', 'Off');
        ini_set('log_errors', 'On');
        ini_set('error_log',ROOT.DS.'tmp'.DS.'log'.DS.'report.log');
    }
}

function stripSlash($value){
    $value = is_array($value) ? array_map('stripSlash', $value) : stripslashes($value);
    return $value;
}
function rmMagicQuotes(){
    if(get_magic_quotes_gpc()){
        $_GET = stripSlash($_GET);
        $_POST = stripSlash($_POST);
        $_COOKIE = stripSlash($_COOKIE);
    }
}
function unregisterGlobal(){
    if(ini_get('register_globals')){
        $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST','_SERVER','_ENV','_FILES');
        foreach ($array as $value) {
            foreach ($GLOBALS[$value] as $key => $value) {
                if($value === $GLOBALS[$key]){
                    unset($GLOBALS[$key]);
                }
                
            }
            
        }
    }
}
function callHook(){
    global $url;
    
    $urlArray = array();
    $urlArray = explode('/', $url);
    
    $controller = $urlArray[0];
    array_shift($urlArray);
    $action = $urlArray[0];
    array_shift($urlArray);
    $queryString = $urlArray;
    
    $controllerName = $controller;
    $controller = ucwords($controller);
    $model = rtrim($controller,'s');
    $controller .= 'Controller';
    $dispatch = new $controller($model, $controllerName, $action);
    
    if((int)  method_exists($controller, $action)){
        call_user_func_array(array($dispatch,$action),$queryString);
    }  else {
        echo 'testerror1';
        
    }
    
}
function __autoload($className){
    if(file_exists(ROOT.DS.'lib'.DS.  strtolower($className).'.class.php')){
        require_once (ROOT.DS.'lib'.DS.  strtolower($className).'.class.php');
    }elseif (file_exists(ROOT.DS.'application'.DS.'controllers'.DS.  strtolower($className).'.php')) {
        require_once (ROOT.DS.'application'.DS.'controllers'.DS.  strtolower($className).'.php');
    }elseif (file_exists(ROOT.DS.'application'.DS.'models'.DS.  strtolower($className).'.php')) {
        require_once (ROOT.DS.'application'.DS.'models'.DS.  strtolower($className).'.php');
    }else{
        echo 'testerror2';
    }
}
Reportview();
rmMagicQuotes();
unregisterGlobal();
callHook();