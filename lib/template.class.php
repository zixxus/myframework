<?php

class Template {

    protected $var = array();
    protected $_controller;
    protected $_action;

    function __construct($controller, $action) {
        $this->_controller = $controller;
        $this->_action = $action;
    }

    function set($name, $val) {
        $this->var[$name] = $val;
    }

    function render() {
        extract($this->var);

        if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'header.php')) {
            include (ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'header.php');
        } else {
            include (ROOT . DS . 'application' . DS . 'views' . DS . 'header.php');
        }


        include (ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . $this->_action . '.php');


        if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'footer.php')) {
            include (ROOT . DS . 'application' . DS . 'views' . DS . $this->_controller . DS . 'footer.php');
        } else {
            include (ROOT . DS . 'application' . DS . 'views' . DS . 'footer.php');
        }
    }

    function renderTemplate($templ) {
        if ($templ != null) {
            $template = ROOT.DS.'application'.DS.'layout'.DS.$templ.'.php';
            if(!empty($template) && file_exists($template)) {
                include $template;
            }
        }
    }

}
