<?php

function get_css_public($address) {
    echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . 'public/css/' . $address;
}

function get_js_public($address) {
    echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . 'public/js/' . $address;
}

function get_img_public($address) {
    echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . 'public/image/' . $address;
}

function gotothefile($where, $text) {
    $serv = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    echo '<a href=" '.$serv. $where .'">'. $text . '</a>';
}
