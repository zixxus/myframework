<?php

class SqlGet {

    protected $_dbHandle;
    protected $_result;

    function connect($server, $username, $password, $dbname) {
        $this->_dbHandle = @mysql_connect($server, $username, $password);
        if ($this->_dbHandle != 0) {
            if (mysql_select_db($dbname, $this->_dbHandle)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function disconnect() {
        if (@mysql_close($this->_dbHandle) != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function selectall() {
        $query = 'select * from `' . $this->_table . '`';
        return $this->query($query);
    }

    function select($id) {
        $query = 'select * from `' . $this->_table . '` where id =\'' . mysql_real_escape_string($id) . '\'';
        return $this->query($query, 1);
    }

    function query($query, $singleres = 0) {
        $this->_result = mysql_query($query, $this->_dbHandle);
        if (preg_match('/select/i', $query)) {
            $result = array();
            $table = array();
            $field = array();
            $temp = array();
            $numfield = mysql_num_fields($this->_result);
            for ($i = 0; $i < $numfield; ++$i) {
                array_push($table, mysql_field_table($this->_result, $i));
                array_push($field, mysql_field_name($this->_result, $i));
            }
            while ($row = mysql_fetch_row($this->_result)) {
                for ($i = 0; $i < $numfield; ++$i) {
                    $table[$i] = trim(ucfirst($table[$i]), 's');
                    $temp[$table[$i]][$field[$i]] = $row[$i];
                }
                if ($singleres == 1) {
                    mysql_free_result($this->_result);
                    return $temp;
                }
                array_push($result, $temp);
            }
            mysql_free_result($this->_result);
            return($result);
        }
    }
    function getnumrow(){
        return mysql_num_rows($this->_result);
        
    }
    function freeresult(){
        return mysql_free_result($this->_result);
    }
    function geterror_mysql(){
        return mysql_error($this->_dbHandle);
    }

}
