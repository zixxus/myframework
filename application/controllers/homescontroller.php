<?php

class HomesController extends Controller{
    public function __construct($model, $controller, $action) {
        parent::__construct($model, $controller, $action);
    }
    public function index(){
        $this->set('title','Default HomePage');
    }
}