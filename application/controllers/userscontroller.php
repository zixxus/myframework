<?php

class UsersController extends Controller {

    function viewdata($id) {
        $this->set('todo', $this->User->select($id));
    }

    function viewall() {
        $this->set('viewall', $this->User->selectAll());
    }

}
